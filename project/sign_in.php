<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" content="text/javascript">
	<title>ULOCK Senior Design Project</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">

	<!--script that handles the actual typing>
	<script type="text/javascript" src="scripts/typing.js"></script-->

</head>
<body>
	<div id="preheader">
      <ul>
          <li><a href="http://www.students.uci.edu/">Students</a></li>
          <li><a href="http://alumni.uci.edu/">Alumni</a></li>
          <li><a href="http://snap.uci.edu/">Faculty &amp; Staff</a></li>
          <li><a href="http://parents.uci.edu/">Parents</a></li>
          <li ><a href="sign_in.php">Sign In</a></li>      
      </ul>
	</div> 
	<div id="container_header">
		<div>
			<a href="http://www.uci.edu"><img id="logo_uci" src="images/uci_logo.png"></a>
			<!--img id="logo_ulock" src="images/uci_logo.png"-->
			<H1 id="logo_ulock">ULOCK</H1>
		</div>  	
	</div>
  	<div id="navigation">
    	<ul>
	      	<li><a href="index.php">Home</a></li>
	      	<li><a href="check_sign_in.php">My Account</a></li>
	      	<li><a href="advisor.php">Advisor</a></li>
	      	<li><a href="about.php">About</a></li>
        	<li><a href="progress.php">Progress</a></li>      
    	</ul>
	</div>
	<div id="background">
		<div id="page">
			<div id="sign_in">
				<div id="sign_in_left">
					<ul>
						<form name="loginform" method="post" action="login.php">
						<li><input name="myusername" class="wide" type="text" id="myusername" placeholder="User Name"></li>
						<li><input name="mypassword" class="wide" type="password" id="mypassword" placeholder="Password"></li>
						<li><input id="keepLogged" type="checkbox"></input>Keep me signed in</li>
						<li><input name="sign_in" class="wide_and_tall" value="Sign In" type="submit" ></li>
						<li class="center"> OR </li>
						</form>
						<li>
							<form name="creataccountform" action="register.php">
								<input type="submit" class="wide_and_tall" value="Create Account">
							</form>
						</li>
					</ul>
				</div>
				<div id="sign_in_right"> <img id="sign_in_image" src="images/sign_in_image.png"></div>
			</div>
			
		</div>
	</div>
	
<body>