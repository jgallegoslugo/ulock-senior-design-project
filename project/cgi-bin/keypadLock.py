import MotorController as MC
import GPIOKeyPad as KP
import time
from time import sleep
import MySQLdb as mdb
import sys

def digit():
	#Loop wile waiting for a keypress
	key = None
	while key ==None:
		key = KP.keypad().getKey()
	return key

def lockDoor(user):
	mc = MC.MotorController(7, 27, 25, KP.keypad().getGPIO())
	mc.lock(.8)
	f = open('/var/www/status.txt','w+')
	f.write('locked')
	f.close()
	log('lock',user)

def log(action,user):
	year = time.strftime("%Y")
	month = time.strftime("%m")
	date = time.strftime("%d")
	hour = time.strftime("%H")
	minute = time.strftime("%M")
	con = mdb.connect('localhost', 'root', 'pi', 'ulock');
	cur = con.cursor()
	try:
		print 'in log insert'
		cur.execute("""INSERT INTO log (lockid, username, year, month, date, hour, minute, action) VALUES ('1',%s,%s,%s,%s,%s,%s,%s)""", (user,year, month, date, hour, minute, action))
		con.commit()
	except:
		con.rollback()
	con.close()
	
	

def unlockDoor(user):
	mc = MC.MotorController(7, 27, 25, KP.keypad().getGPIO())
	mc.unlock(.8)
	f = open('/var/www/status.txt','w+')
	f.write('unlocked')
	f.close()
	log('unlock',user)

def checkToLock(digit):
	return digit == "#"

def reset(digit):
	return digit == "*"
	
def checkSQL(password):
	con = mdb.connect('localhost', 'root', 'pi', 'ulock');
	cur = con.cursor()
	temptPw = password
	try:
		cur.execute("""SELECT username FROM lockpassword WHERE lockid = 1 AND password = %s""", (temptPw))
		con.commit()
		pinPw = cur.fetchall()
		for row in pinPw:
			pinPW = row[0]
			return pinPW
	except:
		con.rollback()
	finally:
		con.close()

def doLogic():
	#wakes up the keypad
	checkFlag = digit();
	sleep(.5)
	if checkToLock(checkFlag):
		lockDoor('lock')
		return
		
	if reset(checkFlag):
		print "Please Enter a 4 Digit Password: "
		d1 = digit()
		print "Digit 1: %s" %d1
		sleep(.5)
		if(reset(d1)):
			return
		d2 = digit()
		print "Digit 2: %s" %d2
		sleep(.5)
		if(reset(d2)):
			return
		d3 = digit()
		print "Digit 3: %s" %d3
		sleep(.5)
		if(reset(d3)):
			return
		d4 = digit()
		print "Digit 4: %s" %d4
		sleep(.5)
		if(reset(d4)):
			return
		
		pinPW = None
		temptPw = "%s%s%s%s" %(d1,d2,d3,d4)
		print temptPw
		pinPW = checkSQL(temptPw)
			
		if pinPW is not None:
			unlockDoor(pinPW)
		else:
			print "Wrong Password"
				
while True:
	doLogic()
			