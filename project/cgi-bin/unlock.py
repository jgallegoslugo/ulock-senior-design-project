import time
import MotorController as MC
import GPIOKeyPad as KP

#currently using pins 7 and 27, and enable 25
mc = MC.MotorController(7, 27, 25, KP.keypad().getGPIO())

mc.unlock(.8)  

mc.cleanup()
