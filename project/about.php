<?php
	function signout()
	{
		session_start();
		$myusername = $_SESSION['myusername'];
		if($myusername!=NULL)
		{
			echo '<li ><a href="sign_out.php">Sign Out</a></li>';
		}
		else
		{
			echo '<li ><a href="sign_in.php">Sign In</a></li>';
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" content="text/javascript">
	<title>ULOCK Senior Design Project</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">

	<!--script that handles the actual typing>
	<script type="text/javascript" src="scripts/typing.js"></script-->

</head>
<body>
	<div id="preheader">
      <ul>
          <li><a href="http://www.students.uci.edu/">Students</a></li>
          <li><a href="http://alumni.uci.edu/">Alumni</a></li>
          <li><a href="http://snap.uci.edu/">Faculty &amp; Staff</a></li>
          <li><a href="http://parents.uci.edu/">Parents</a></li>
          <? signout() ?>
      </ul>
	</div> 
	<div id="container_header">
		<div>
			<a href="http://www.uci.edu"><img id="logo_uci" src="images/uci_logo.png"></a>
			<!--img id="logo_ulock" src="images/uci_logo.png"-->
			<H1 id="logo_ulock">ULOCK</H1>
		</div>  	
	</div>
  	<div id="navigation">
    	<ul>
	      	<li><a href="index.php">Home</a></li>
	      	<li><a href="check_sign_in.php">My Account</a></li>
	      	<li><a href="advisor.php">Advisor</a></li>
	      	<li><a href="about.php">About</a></li>
        	<li><a href="progress.php">Progress</a></li>      
    	</ul>
	</div>
	<div id="background">
		<div id="page">
			<div id="about">
				<img id="jason" src="images/jason.png"><br>
				<p>
				Jason is an undergraduate Computer Science and Engineering student and also the team leader for Blueprint.  Communication and programming skills are his strengths that will be performing in the project development. He has taken an Android application project course as one of his personal interest. Jason is assigned with Android application development and managing the project as his responsibilities.
				</p>
			</div>
			<HR>
			<div id="about">
				<img id="chi" src="images/chi.png">
				<p>	
				Chi Wu serves as the software project engineer for ULock Keyless Entry project and specializes on web development. Chi is entering his final year at the University of California, Irvine as a Computer Science & Engineering  major. Chi came to Blueprint after being an intern with LaunchPad. During his intern in LaunchPad, Chi gained knowledge on web development and design, one in which he made a tool on the cloud as a version controller for the company. Chi has personally headed up multiple successful design projects for LaunchPad and came highly recommended from his supervisors. His work experience brings an unique and innovative view to the Blueprint
				</p>
			</div>
			<HR>
			<div id="about">
				<img id="kevin" src="images/kevin.png">
				<p>	
				Kevin Chien serves as the Electrical Engineer for ULock Keyless Entry project. He is entering his senior year at the Henry Samuel School of Engineering, University of California, Irvine. Kevin brings a variety of experience, as well as dedication into the field. Specializing in electrical circuits and some mechanical background, Kevin has the knowledge and assets in providing the team with a more efficient door lock circuits design and development. With the help of his past studies and design projects, he plans to use this knowledge in the industry.
				</p>
			</div>
			<HR>
			<div id="about">
				<img id="jose" src="images/jose.png"><br>
				<p>	
				Jose Gallegos has placed an emphasis on software-hardware interactions during his time at the University of California, Irvine while studying Computer Science Engineering. He has tutored in many campus facilities such as CAMP/Mesa which has exposed him to many different programming languages. This exposure will be put to practice by choosing programming languages which will interact in the most efficient way with the overall system.
				</p>
			</div>
		</div>
	</div>
	
<body>