<?php
function signout()
	{
		session_start();
		$myusername = $_SESSION['myusername'];
		if($myusername!=NULL)
		{
			echo '<li ><a href="sign_out.php">Sign Out</a></li>';
		}
		else
		{
			echo '<li ><a href="sign_in.php">Sign In</a></li>';
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" content="text/javascript">
	<title>ULOCK Senior Design Project</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">

	<!--script that handles the actual typing>
	<script type="text/javascript" src="scripts/typing.js"></script-->

</head>
<body>
	<div id="preheader">
      <ul>
          <li><a href="http://www.students.uci.edu/">Students</a></li>
          <li><a href="http://alumni.uci.edu/">Alumni</a></li>
          <li><a href="http://snap.uci.edu/">Faculty &amp; Staff</a></li>
          <li><a href="http://parents.uci.edu/">Parents</a></li>
          <? signout() ?>     
      </ul>
	</div> 
	<div id="container_header">
		<div>
			<a href="http://www.uci.edu"><img id="logo_uci" src="images/uci_logo.png"></a>
			<!--img id="logo_ulock" src="images/uci_logo.png"-->
			<H1 id="logo_ulock">ULOCK</H1>
		</div>  	
	</div>
  	<div id="navigation">
    	<ul>
	      	<li><a href="index.php">Home</a></li>
	      	<li><a href="check_sign_in.php">My Account</a></li>
	      	<li><a href="advisor.php">Advisor</a></li>
	      	<li><a href="about.php">About</a></li>
        	<li><a href="progress.php">Progress</a></li>      
    	</ul>
	</div>
	<div id="background">
		<div id="under_construction">
			<div>
				<form name="form1" method="post" action="lockpasswordsubmit.php">
					Name of the user: <input name="user" type="text" id="user"></br>
					Passcode: <input name="passcode" type="text" id="passcode"></br>
					Start Time: Year
					<select name="syear">
						<option value="2014">2014</option>
						<option value="2015">2015</option>
						<option value="2016">2016</option>
					</select>
					Month <select name="smonth">
						<option value="01">January</option>
						<option value="02">February</option>
						<option value="03">March</option>
						<option value="04">April</option>
						<option value="05">May</option>
						<option value="06">June</option>
						<option value="07">July</option>
						<option value="08">August</option>
						<option value="09">September</option>
						<option value="10">October</option>
						<option value="11">November</option>
						<option value="12">December</option>
					</select>
					Date <select name="sdate">
						<option value="01">1</option>
						<option value="02">2</option>
						<option value="03">3</option>
						<option value="04">4</option>
						<option value="05">5</option>
						<option value="06">6</option>
						<option value="07">7</option>
						<option value="08">8</option>
						<option value="09">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
					</select>
					Hour <select name="shour">
						<option value="00">00</option>
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
					</select>
					Minute <select name="sminute">
						<option value="00">00</option>
						<option value="15">15</option>
						<option value="30">30</option>
						<option value="45">45</option>
					</select>
					</br>
					End Time: Year
					<select name="eyear">
						<option value="2014">2014</option>
						<option value="2015">2015</option>
						<option value="2016">2016</option>
					</select>
					Month <select name="emonth">
						<option value="01">January</option>
						<option value="02">February</option>
						<option value="03">March</option>
						<option value="04">April</option>
						<option value="05">May</option>
						<option value="06">June</option>
						<option value="07">July</option>
						<option value="08">August</option>
						<option value="09">September</option>
						<option value="10">October</option>
						<option value="11">November</option>
						<option value="12">December</option>
					</select>
					Date <select name="edate">
						<option value="01">1</option>
						<option value="02">2</option>
						<option value="03">3</option>
						<option value="04">4</option>
						<option value="05">5</option>
						<option value="06">6</option>
						<option value="07">7</option>
						<option value="08">8</option>
						<option value="09">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
					</select>
					Hour <select name="ehour">
						<option value="00">00</option>
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
					</select>
					Minute <select name="eminute">
						<option value="00">00</option>
						<option value="15">15</option>
						<option value="30">30</option>
						<option value="45">45</option>
					</select>
					</br>
					<input type="submit" name="Submit" value="Submit">
				</form>
			</div>
		</div>
	</div>
	
<body>