<?php
	session_start();
	$checklogin = $_SESSION['myusername'];
	$deletedPasscode = $_SESSION['deleted'];
	if($checklogin == null)
	{
		header('Location:check_sign_in.php');
	}

	function checkdelete()
	{
		session_start();
		$deleteMessage = '';
		$deleteAction = $_SESSION['deleteAction'];
		if($deleteAction == NULL || $deleteAction == FALSE)
		{
			
		}
		else
		{
			$deleteMessage = $_SESSION['deleted'];
			$deleteMessage = "Passcode: " . $deleteMessage;
		}
		$_SESSION['deleteAction'] = NULL;
		echo $deleteMessage;
	}
	
	function signout()
	{
		session_start();
		$myusername = $_SESSION['myusername'];
		if($myusername!=NULL)
		{
			echo '<li ><a href="sign_out.php">Sign Out</a></li>';
		}
		else
		{
			echo '<li ><a href="sign_in.php">Sign In</a></li>';
		}
	}
	
	function showActivePassword()
	{
		session_start();
		$myusername = $_SESSION['myusername'];
		$lockid = $_SESSION['lockid'];
		$host="localhost"; // Host name 
		$username="root"; // Mysql username 
		$password="pi"; // Mysql password 
		$db_name="ulock"; // Database name 
		$tbl_name="lockpassword"; // Table name
		$tbl_nameTwo="passwordtime";
		$output = "Active passcode: 
				<table border='1' style='width:300px'>
				<tr>
					<td>User</td>
					<td>Passcode</td>
					<td>Expire Time</td>
				</tr>
				";
		
		mysql_connect("$host", "$username", "$password")or die("cannot connect"); 
		mysql_select_db("$db_name")or die("cannot select DB");
		
		$sqlPrimary ="SELECT username, password FROM $tbl_name WHERE lockid='$lockid' AND username='primary'";
		$primaryQ = mysql_query($sqlPrimary);
		
		while($row = mysql_fetch_assoc($primaryQ))
		{
			$output .= "<tr> <td>";
			$output .= "Primary";
			$output .= "</td> <td>";
			$output .= $row['password'];
			$output .= "</td><td>";
			$output .= "Permanent";
			$output .= "</td> </tr>";
		}
		
		mysql_free_result($primaryQ);
		
		$sql="SELECT a.username AS username, a.password AS password, b.eyear AS eyear, b.emonth AS emonth, b.edate AS edate, b.ehour AS ehour, b.eminute AS eminute FROM $tbl_name AS a, $tbl_nameTwo AS b WHERE a.password = b.passcode AND a.lockid='$lockid'";
		$firstQuery = mysql_query($sql);
		
		while($row = mysql_fetch_assoc($firstQuery))
		{
			$output .= "<tr> <td>";
			$output .= $row['username'];
			$output .= "</td> <td>";
			$output .= $row['password'];
			$output .= "</td><td>";
			$output .= $row['eyear'];
			$output .= "-";
			$output .= $row['emonth'];
			$output .= "-";
			$output .= $row['edate'];
			$output .= "  ";
			$output .= $row['ehour'];
			$output .= ":";
			$output .= $row['eminute'];
			$output .= "</td> </tr>";
		}
		
		mysql_free_result($firstQuery);
		
		$sqlDemo="SELECT a.username AS username, a.password AS password, b.username AS demoname FROM $tbl_name AS a, demo AS b WHERE a.username = b.username";
		$DemoQuery = mysql_query($sqlDemo);
		
		while($row = mysql_fetch_assoc($DemoQuery))
		{
			$output .= "<tr> <td>";
			$output .= $row['username'];
			$output .= "</td> <td>";
			$output .= $row['password'];
			$output .= "</td><td>";
			$output .= "DemoUser";
			$output .= "</td> </tr>";
		}
		
		echo $output;
	}
	
?>

<html>
<head>
	<meta charset="UTF-8" content="text/javascript">
	<title>ULOCK Login</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">

	<!--script that handles the actual typing>
	<script type="text/javascript" src="scripts/typing.js"></script-->

</head>
<body>
	<div id="preheader">
      <ul>
          <li><a href="http://www.students.uci.edu/">Students</a></li>
          <li><a href="http://alumni.uci.edu/">Alumni</a></li>
          <li><a href="http://snap.uci.edu/">Faculty &amp; Staff</a></li>
          <li><a href="http://parents.uci.edu/">Parents</a></li>
		  <? signout() ?>
      </ul>
	</div> 
	<div id="container_header">
		<div>
			<a href="http://www.uci.edu"><img id="logo_uci" src="images/uci_logo.png"></a>
			<!--img id="logo_ulock" src="images/uci_logo.png"-->
			<H1 id="logo_ulock">ULOCK</H1>
		</div>  	
	</div>
  	<div id="navigation">
    	<ul>
	      	<li><a href="index.php">Home</a></li>
	      	<li><a href="check_sign_in.php">My Account</a></li>
	      	<li><a href="advisor.php">Advisor</a></li>
	      	<li><a href="about.php">About</a></li>
        	<li><a href="progress.php">Progress</a></li>      
    	</ul>
	</div>
	<div id="background">
		<div id="under_construction">
			<div id="page">
				<form name="delete_passcode" method="post" action="deletePasscode.php" onsubmit="return confirm('Are you sure you want to delete')">
					Delete: Passcode 
					<input name="passcode" type="text" id="passcode">
					<input type="submit" name="Submit" value="Submit">
				</form>
				<? showActivePassword() ?>
				</br>
				<? checkdelete() ?>
			</div>
		</div>
	</div>	
</body>
</html>