<?php
session_start();
$myusername = $_SESSION['myusername'];

if($myusername == null)
{
	header('Location: check_sign_in.php');
}

function signout()
	{
		session_start();
		$myusername = $_SESSION['myusername'];
		if($myusername!=NULL)
		{
			echo '<li ><a href="sign_out.php">Sign Out</a></li>';
		}
		else
		{
			echo '<li ><a href="sign_in.php">Sign In</a></li>';
		}
	}

//session_start();
//$myusername = $_SESSION['myusername'];
$status = '';
$rstatus = '';
$file = fopen("status.txt", "r") or exit("Can not get lock status");
$picture='';
while(!feof($file))
{
	$status .= fgets($file);
}

$status = preg_replace('/\s+/', '', $status);

$close = fclose($file);

if($status == "locked")
{
	$rstatus = "unlock";
}
else
{
	$rstatus = "lock";
}
$picture = '/' . $rstatus . "ed.png";

$_SESSION['status'] = $rstatus;
//$_SESSION['logname'] = $myusername;
?>

<html>
<head>
	<meta charset="UTF-8" content="text/javascript">
	<title>ULOCK Senior Design Project</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">

	<!--script that handles the actual typing>
	<script type="text/javascript" src="scripts/typing.js"></script-->

</head>
<body>
	<div id="preheader">
      <ul>
          <li><a href="http://www.students.uci.edu/">Students</a></li>
          <li><a href="http://alumni.uci.edu/">Alumni</a></li>
          <li><a href="http://snap.uci.edu/">Faculty &amp; Staff</a></li>
          <li><a href="http://parents.uci.edu/">Parents</a></li>
          <? signout() ?>
      </ul>
	</div> 
	<div id="container_header">
		<div>
			<a href="http://www.uci.edu"><img id="logo_uci" src="images/uci_logo.png"></a>
			<!--img id="logo_ulock" src="images/uci_logo.png"-->
			<H1 id="logo_ulock">ULOCK</H1>
		</div>  	
	</div>
  	<div id="navigation">
    	<ul>
	      	<li><a href="index.php">Home</a></li>
	      	<li><a href="check_sign_in.php">My Account</a></li>
	      	<li><a href="advisor.php">Advisor</a></li>
	      	<li><a href="about.php">About</a></li>
        	<li><a href="progress.php">Progress</a></li>      
    	</ul>
	</div>
	<div id="background">
		<div id="under_construction">
			<H1>Welcome, <?php echo $myusername ?>. </H1>
			<H1>The lock status is: <?php echo $status ?>.</br>
			Please click <a href="lockchange.php"><img src=<?php echo $picture?> height="40" width="40"/></a> to <?php echo $rstatus ?>
			</H1>
			<H1><a href="log.php">Show Log</a></H1>
			<H1><a href="activePasscode.php">Show active passcode</a></H1>
			<H1><a href="lockpasswordsetting.php">Set Lock Passcode</a></H1>
			<H1><a href="lockpasswordsettingDemo.php">Set Lock Passcode - Senior Design Day</a></H1>
		</div>
	</div>
	
<body>