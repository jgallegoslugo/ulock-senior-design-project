<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" content="text/javascript">
	<title>ULOCK Senior Design Project</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">

	<!--script that handles the actual typing>
	<script type="text/javascript" src="scripts/typing.js"></script-->

</head>
<body>
	<div id="preheader">
      <ul>
          <li><a href="http://www.students.uci.edu/">Students</a></li>
          <li><a href="http://alumni.uci.edu/">Alumni</a></li>
          <li><a href="http://snap.uci.edu/">Faculty &amp; Staff</a></li>
          <li><a href="http://parents.uci.edu/">Parents</a></li>
          <li ><a href="sign_in.php">Sign In</a></li>      
      </ul>
	</div> 
	<div id="container_header">
		<div>
			<a href="http://www.uci.edu"><img id="logo_uci" src="images/uci_logo.png"></a>
			<!--img id="logo_ulock" src="images/uci_logo.png"-->
			<H1 id="logo_ulock">ULOCK</H1>
		</div>  	
	</div>
  	<div id="navigation">
    	<ul>
	      	<li><a href="index.php">Home</a></li>
	      	<li><a href="check_sign_in.php">My Account</a></li>
	      	<li><a href="advisor.php">Advisor</a></li>
	      	<li><a href="about.php">About</a></li>
        	<li><a href="progress.php">Progress</a></li>      
    	</ul>
	</div>
	<div id="background">
		<div id="under_construction">
			<table width="300" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
			<tr>
			<form name="form1" method="post" action="registersubmit.php">
			<td>
			<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF">
			<tr>
			<td colspan="3"><strong>Register </strong></td>
			</tr>
			<tr>
			<td width="78">Username</td>
			<td width="6">:</td>
			<td width="294"><input name="myusername" type="text" id="myusername"></td>
			</tr>
			<tr>
			<td>Password</td>
			<td>:</td>
			<td><input name="mypassword" type="password" id="mypassword"></td>
			</tr>
			<tr>
			<td>Re-enter Password</td>
			<td>:</td>
			<td><input name="passwordcheck" type="password" id="passwordcheck"></td>
			</tr>
			<tr>
			<td>Lock ID</td>
			<td>:</td>
			<td><input name="lockid" type="text" id="lockid"></td>
			</tr>
			<tr>
			<td>Lock Password(4 digits)</td>
			<td>:</td>
			<td><input name="lockpassword" type="password" id="lockpassword"></td>
			</tr>
			<tr>
			<td></td>
			<td></td>
			<td><input type="submit" name="Submit" value="Submit"></td>
			</tr>
			</table>
			</td>
			</form>
			</tr>
			</table>
		</div>
	</div>
	
</body>
</html>