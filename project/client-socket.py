#!/usr/bin/python

import cgi
import socket 

form = cgi.FieldStorage()

#establish an FTP connection
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

ip = form['IP'].value

#connect to the Pi ip address at the time
#IP: 169.234.27.100
#PORT: 6335
s.connect((ip, 6335))

#Message to send to Pi 
msg = 'blink led'

#send the message blink led
sent = s.send(msg)

#close socket connection
s.close()
