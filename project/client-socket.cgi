#!/usr/bin/python

import cgi, cgitb
import socket 

cgitb.enable()	
form = cgi.FieldStorage()
print "Content-type: text/html"

#establish an FTP connection
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

ip = form['IP'].value

html = '''
<title> IP </title>
<p> %s</p>
<HR>
'''

#connect to the Pi ip address at the time
#IP: 169.234.27.100
#PORT: 6335
s.connect((ip, 6335))

#Message to send to Pi 
msg = 'blink led'

#send the message blink led
sent = s.send(msg)

#close socket connection
s.close()
print 
print html % form.getvalue('IP')
