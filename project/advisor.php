<?php
	function signout()
	{
		session_start();
		$myusername = $_SESSION['myusername'];
		if($myusername!=NULL)
		{
			echo '<li ><a href="sign_out.php">Sign Out</a></li>';
		}
		else
		{
			echo '<li ><a href="sign_in.php">Sign In</a></li>';
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" content="text/javascript">
	<title>ULOCK Senior Design Project</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">

	<!--script that handles the actual typing>
	<script type="text/javascript" src="scripts/typing.js"></script-->

</head>
<body>
	<div id="preheader">
      <ul>
          <li><a href="http://www.students.uci.edu/">Students</a></li>
          <li><a href="http://alumni.uci.edu/">Alumni</a></li>
          <li><a href="http://snap.uci.edu/">Faculty &amp; Staff</a></li>
          <li><a href="http://parents.uci.edu/">Parents</a></li>
          <? signout() ?>
      </ul>
	</div> 
	<div id="container_header">
		<div>
			<a href="http://www.uci.edu"><img id="logo_uci" src="images/uci_logo.png"></a>
			<!--img id="logo_ulock" src="images/uci_logo.png"-->
			<H1 id="logo_ulock">ULOCK</H1>
		</div>  	
	</div>
  	<div id="navigation">
    	<ul>
	      	<li><a href="index.php">Home</a></li>
	      	<li><a href="check_sign_in.php">My Account</a></li>
	      	<li><a href="advisor.php">Advisor</a></li>
	      	<li><a href="about.php">About</a></li>
        	<li><a href="progress.php">Progress</a></li>      
    	</ul>
	</div>
	<div id="background">
		<div id="page">
			<div id="advisor">
				<div id="advisor_header">
					<img id="advisor_image" src="images/advisor.png">
					<H1>Hamid Jafarkhani</H1>
					Chancellor's Professor, <a href="http://www.eng.uci.edu/dept/eecs">Electrical Engineering and Computer Science</a>
					<br>
					Director, <a href="http://www.cpcc.uci.edu/">Center for Pervasive Communications and Computing</a>
					<H2>Education:</H2>
						B.S., Tehran University, Electrical Engineering, 1989 <br>
						M.S., University of Maryland,College Park, Electrical Engineering, 1994 <br>
						Ph.D., University of Maryland,College Park, Electrical Engineering, 1997 <br>
				</div>
				<br>
				<H2>Address:</H2> 
					The Henry Samueli School of Engineering <br>
					University of California, Irvine <br>
					Irvine, CA 92697-2625 
				<H2>ZOT Code: 2625</H2>
				<H2>Location:</H2> 
					Office: EH 4213 <br>
					CPCC Lab: ET 408 
				<H2>Phone:</H2> 
					(949) 824-1755 Office <br>
					Fax: (949) 824-2321 <br>
					Email: hamidj@uci.edu <br>
				<H2>Research:</H2> 
					<!--p style="text-indent: 5em;"-->
					<p>
					Dr. Jafarkhani is interested in communications theory, with an emphasis on coding.
					He is well known as one of the inventors of space-time block coding, which is widely used to improve wireless transmission quality. He is currently concentrating on the theoretical and practical challenges of designing systems that use multiple antennas. One of these challenges is to manage the tradeoff between diversity and rate from an information theory perspective. Another is to design practical coding schemes that satisfy those limits using signal-processing methods.
					</p>
					<p style="text-indent: 5em;">
					Dr. Jafarkhani also has been involved in developing data compression algorithms especially for image and video coding. His recent work in this field concerns the transmission of multimedia information over wireless networks and the Internet. He is interested in developing new coding schemes and network protocols that improve end-to-end recovery and enhance the quality of service for video.
					</p>
				<H2>Publications:</H2>
				<a href="http://www.eng.uci.edu/files/Jafarkhani-Selected-Publication-List.pdf">Selected Publications List</a><br>
				<a href="http://www.eng.uci.edu/files/Jafarkhani-Super-Quasi-Orthogonal-Jan-2005.pdf">Super-Quasi-Orthogonal Space-Time Trellis Codes</a> IEEE Transactions on Wireless Communications<br>
				<a href="http://www.eng.uci.edu/files/Jafarkhani-Super-Orthogonal-Space-Time-April-2003.pdf">Super-Orthogonal Space-Time Trellis Codes</a> IEEE Transactions on Information Theory<br>
				<a href="http://www.eng.uci.edu/files/Jafarkhani-Design-of-Successively-Refinable-July-1999.pdf">Design of Successively Refinable Trellis Coded Quantizers</a> IEEE Transactions on Information Theory <br>
				<a href="http://www.eng.uci.edu/files/Jafarkhani-Space-Time-Block-Codes-July-1999.pdf">Space-Time Block Codes from Orthogonal Designs</a> IEEE Transactions on Information Theory
				<H2>Research topics:</H2> 
					Communication Theory
				<H2>Links:</H2> 
					<a href="http://newport.eecs.uci.edu/~hamidj/">http:&#47;&#47;newport.eecs.uci.edu&#47;~hamidj&#47;</a>
			</div>
		</div>
	</div>
	
<body>