<?php
	function signout()
	{
		session_start();
		$myusername = $_SESSION['myusername'];
		if($myusername!=NULL)
		{
			echo '<li ><a href="sign_out.php">Sign Out</a></li>';
		}
		else
		{
			echo '<li ><a href="sign_in.php">Sign In</a></li>';
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" content="text/javascript">
	<title>ULOCK Senior Design Project</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">

	<!--script that handles the client socket-->
	<!--script type="text/javascript" src="scripts/client-socket.js"></script-->

</head>
<body>
	<div id="preheader">
      <ul>
          <li><a href="http://www.students.uci.edu/">Students</a></li>
          <li><a href="http://alumni.uci.edu/">Alumni</a></li>
          <li><a href="http://snap.uci.edu/">Faculty &amp; Staff</a></li>
          <li><a href="http://parents.uci.edu/">Parents</a></li>
          <? signout() ?>
      </ul>
	</div> 
	<div id="container_header">
		<div>
			<a href="http://www.uci.edu"><img id="logo_uci" src="images/uci_logo.png"></a>
			<!--img id="logo_ulock" src="images/uci_logo.png"-->
			<H1 id="logo_ulock">ULOCK</H1>
		</div>  	
	</div>
	<div id="navigation">
    	<ul>
	      	<li><a href="index.php">Home</a></li>
	      	<li><a href="check_sign_in.php">My Account</a></li>
	      	<li><a href="advisor.php">Advisor</a></li>
	      	<li><a href="about.php">About</a></li>
        	<li><a href="progress.php">Progress</a></li>      
    	</ul>
	</div>
	<div id="background">
		<div id="page">
			<div id="home">
			<br>
			<H2 style="width: 800px; margin: 0 auto; color:white;">Welcome to ULOCK</H2>
			<p style="color:white;">The previous decade has driven society to place an importance in security more than ever. You can now install many security components for your home or business such as cameras and monitoring services, but access to properties has remained virtually unchanged. If someone has to enter your home or business you have to either: be at the property, give the person a key, or give them an access code. All these solutions are inconvenient and can compromise safety because you risk losing keys or sharing access code.</p>
			<p style="color:white;">We are going to revolutionize the way you think of keys by eliminating the need for one! This proposal aims to develop a keyless entry system that can be accessed by a password or by means of internet connectivity. With this system you will be able to share temporary guest passwords for your friends and special guests such as maids and construction workers. You will no longer have to hold on to extra keys and much less worry about anyone losing them. You will also be able to buzz someone in from a remote location in case you don’t want the inconvenience of setting temporary passwords. Once this technology is fully implemented, you could potentially be able to grant temporary access to services such as UPS and USPS so that your important packages are not left outside. </p>
			<p style="color:white;">This design is not for everyone. If you live by yourself, don’t have children, or hardly ever have visitors, you would probably not have a need to use all the features that this product has to offer. This product is intended for use in a place with many visitors such as a business place or homes with large families. Although our product will be more expensive than a traditional door lock, it will still be cheaper or about the same price as a brand new cell phone but without the recurring monthly bill.</p>
			<p style="color:white;">Our current competitors are traditional door lock and deadbolt makers such as Schlage and Kwikset. There are also other more advanced keyless entry systems such as Kevo, designed by Kwikset, which allows keyless entry. However, Kevo is only compatible with iPhones or their own personal access cards. This is hardly an improvement for two reasons: it builds a dependency on iPhone products, and there is really no new features provided being that you still have to carry your phone or access card physically to the door. It is also unreasonable to expect users to buy an iPhone if they want to have all the benefits that come with your product. We will eliminate constraints by providing as much means of access as possible in a convenient way regardless of where you are at the moment.</p>
			</div>
		</div>
	</div>
</body>
</html>
