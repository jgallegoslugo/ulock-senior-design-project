#!/usr/bin/python2.7
import cgitb
import time
import MotorController as MC
import GPIOKeyPad as KP

cgitb.enable()

#currently using pins 7 and 27, and enable 25
mc = MC.MotorController(7, 27, 25, KP.keypad().getGPIO())
mc.lock(.8)
time.sleep(2) 

mc.unlock(.8)  

mc.cleanup()
