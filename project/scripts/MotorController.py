import time
import GPIOKeyPad as GPIO

class MotorController:
	
	def __init__(self, in1, in2, enable, GPIO):
		self._GPIO = GPIO
		self._GPIO.setmode(self._GPIO.BCM)
		#set pins to output pins
		self._GPIO.setup(in1, self._GPIO.OUT)
		self._GPIO.setup(in2, self._GPIO.OUT)
		self._GPIO.setup(enable, self._GPIO.OUT)
		#initially set all pins low
		self._GPIO.output(in1, self._GPIO.LOW)
		self._GPIO.output(in2, self._GPIO.LOW)
		self._GPIO.output(enable, self._GPIO.LOW)
		self._in1 = in1
		self._in2 = in2
		self._en = enable

	#time = number of times you want the while loop to run
	def lock(self, duration):

		self._GPIO.output(self._in1, self._GPIO.HIGH)
		self._GPIO.output(self._in2, self._GPIO.LOW)

		self._GPIO.output(self._en, self._GPIO.HIGH)
		time.sleep(duration)

		#set everything to ground once we're done locking
		self._GPIO.output(self._in1, self._GPIO.LOW)
		self._GPIO.output(self._in2, self._GPIO.LOW)
		self._GPIO.output(self._en, self._GPIO.LOW)

	#time = number of times you want the while loop to run
	def unlock(self, duration):

		self._GPIO.output(self._in1, self._GPIO.LOW)
		self._GPIO.output(self._in2, self._GPIO.HIGH)

		self._GPIO.output(self._en, self._GPIO.HIGH)
		time.sleep(duration)

		#set everything to ground once we're done locking
		self._GPIO.output(self._in1, self._GPIO.LOW)
		self._GPIO.output(self._in2, self._GPIO.LOW)
		self._GPIO.output(self._en, self._GPIO.LOW)
	
	def cleanup(self):
		self._GPIO.cleanup()

